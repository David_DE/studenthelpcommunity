var express = require('express'),
    mongoose = require('mongoose'),
    app = module.exports = express(),
    User = require('./models/user.js');

mongoose.connect('mongodb://localhost/communityDB');

app.use(express.bodyParser()); //Needed for req.body.* (or is it?)

app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.session({ secret: 'SECRET' }));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, token');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var api = require('./controllers/api.js');


app.get('/posts', api.getPosts);
app.post('/posts', api.addPost);
app.get('/posts/:_id', api.getPost);
app.delete('/posts/:_id', api.delPost);

app.get('/comments', api.getComments);

app.get('/comments/:_id', api.getCommentsOfPost);
app.post('/comments', api.addComment);

app.get('/users', api.getUsers);
app.post('/users', api.addUser);

app.get('/users/me', api.getUserMe);

app.post('/login', api.login);


app.listen(8080);
console.log('Listening on port 8080...');