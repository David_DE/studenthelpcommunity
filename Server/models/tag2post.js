var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var tag2postSchema = new Schema({
    tag     : { type: Schema.Types.ObjectId, ref: 'Tag' },
    post    : { type: Schema.Types.ObjectId, ref: 'Post' }
});

module.exports = mongoose.model('Tag2PostSchema', tag2postSchema);