var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


var commentSchema = new Schema({
    title       : { type: String, required: true, trim: true },
    content     : { type: String, required: true, trim: true },
    upVotes     : { type: Number, default: 0 },
    downVotes   : { type: Number, default: 0 },
    time        : { type: Date, default: Date.now },
    author      : { type: Schema.Types.ObjectId, ref: 'User' }
    //post        : { type: Schema.Types.ObjectId, ref: 'Post' }
});

module.exports = mongoose.model('Comment', commentSchema);