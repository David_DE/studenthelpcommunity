var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var userSchema = new Schema({
    username    : { type: String, required: true, trim: true },
    email       : { type: String, required: true, trim: true },
    password    : { type: String, required: true },
    currentToken: { type: String, required: true, default: '-'}
});

userSchema.methods.validPassword = function (password) {
    if (this.password == password) {
        return true;
    }

    return false;
}

userSchema.methods.generateToken = function (id) {
    var newToken = this.username + this.password;

    return newToken;
}

module.exports = mongoose.model('User', userSchema);