var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Comment = require('./comment.js'),
    ObjectId = Schema.ObjectId;


var postSchema = new Schema({
    title       : { type: String, required: true, trim: true },
    content     : { type: String, required: true, trim: true },
    upVotes     : { type: Number, default: 0 },
    downVotes   : { type: Number, default: 0 },
    time        : { type: Date, default: Date.now },
    author      : { type: Schema.Types.ObjectId, ref: 'User' },
    comments    : [Comment.Schema]
    //comments    : {type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}
});

module.exports = mongoose.model('Post', postSchema);