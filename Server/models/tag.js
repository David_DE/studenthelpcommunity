var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var tagSchema = new Schema({
    name : { type: String, required: true, trim: true}
});

module.exports = mongoose.model('Tag', tagSchema);