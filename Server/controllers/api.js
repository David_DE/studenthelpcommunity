var User = require('../models/user.js'),
    Comment = require('../models/comment.js'),
    Post = require('../models/post.js'),
    Tag = require('../models/tag'),
    Tag2Post = require('../models/tag2post'),
    mongoose = require('mongoose');

/**
 * Return all available posts
 */
exports.getPosts = function(req, res) { //Queries!
    Post
        .find()
        //.select('-comments -__v')
        .select('-comments.author -comments')
        .populate({
            path: 'author',
            select: '_id username'
        })
        .sort({time: +1})
        .exec(function(error, postdata) {
            if (error) {
                res.send(error);
            } else if (postdata == null) {
                res.send('No posts found');
            } else {
                res.json({
                    posts: postdata
                });

            }
        });
};

/**
 * Return post by its id
 */
exports.getPost = function(req, res) {
    Post.findOne( { _id: req.params._id })
        //.select('-__v')
        .populate({
            path: 'author',
            select: '_id username'
        })
        .exec(function(error, data){
        if (error) {
            res.send(error);
        } else if (data == null) {
            res.send('Data empty');
        } else {
            res.json({
                post: data
            });
        }
    });
};

/**
 * Add a new Post
 */
exports.addPost = function(req, res) {
    validTokenProvided(req, res, function() {
        console.log('6.');
        var p = new Post({
            title: req.body.post.title,
            content: req.body.post.content,
            author: req.body.post.author
        });
        p.save(function(error, data){
            if (error) {
                res.send(error);
            } else if (data == null) {
                res.send('Data empty');
            } else {
                console.log('saved');
                //res.json(201, data);
                res.json(201, {
                    post: data
                });
            }
        });
    });
};

/**
 * Return all available users
 */
exports.getUsers = function(req, res) {
    User
        .find()
        .exec(function(error, data) {
            if (error) {
                res.send(error);
            } else if (data == null) {
                res.send('No posts found');
            } else {
                res.json(data);
            }
        });
};

exports.getUserMe = function(req, res) {
    var userToken = req.body.token || req.param('token') || req.headers.token;

    User.findOne({ currentToken: userToken }, function (err, user) {

        if (err){
            console.log('error: ' + err);
            res.send(403, { error: 'Invalid token. You provided: ' + userToken });
        } else if (user == null) {
            res.send(403, { error: 'Invalid token. You provided: ' + userToken });
        } else {
            res.json(user);
        }
    });
};
/**
 * Add a new User
 */
exports.addUser = function(req, res) {
    var u = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });

    u.save(function(error, data) {
        if (error) {
            res.send(error);
        } else if (data == null) {
            res.send('Data empty');
        } else {
            //res.json(data);
            res.send({
                success: true
            });
        }
    });
};

/**
 * Find post via id and delete it
 * TODO: authentification?
 */
exports.delPost = function(req, res) {
    Post.findByIdAndRemove(req.params._id,
        function(error, data) {
            if (error) {
                res.send(error);
            } else {
                res.send(data);
            }
        }
    );
};

exports.getComments = function(reg, res) {
    console.log('test' + reg.query.ids);

    Comment.find({_id: { $in: reg.query.ids }})
        .exec(function(error, data){
            if (error) {
                res.send(error);
            } else if (data == null) {
                res.send('Data empty');
            } else {
                res.json(200, {
                    comments: data
                });
            }
        });

    //res.send(400, "Error");
};

/**
 * Get all comments of a specific post
 */
exports.getCommentsOfPost = function(req, res) {
    Post.findById(req.params._id)
        .select('comments')
        .populate({
            path: 'post.author',
            select: '_id username'
        })
        .populate('comment')
        .exec(function(error, data){
            if (error) {
                res.send(error);
            } else if (data == null) {
                res.send('Data empty');
            } else {
                res.json({
                    comments: data.comments
                });
            }
        });
};

/**
 * Add a comment to a specific post
 */
exports.addComment = function(req, res) {
    Post.findById(req.body.postId,
        function(error, post){
            if (error) {
                res.send(error);
            } else if (post == null) {
                res.send('Data empty');
            } else {

                var c = new Comment({
                    title: req.body.title,
                    content: req.body.content,
                    author: req.body.author
                });
                c.save(function(error, data){
                    if (error) {
                        res.send(error);
                    } else if (data == null) {
                        res.send('Data empty' + req.body.title);
                    } else {
                        console.log('saved');

                        post.comments.push(c._id);
                        post.save();

                        res.json(201, {
                            comment: c
                        });
                    }
                });
                //res.json(post);
            }
        });
};

exports.login = function(req, res) {

    var username = req.body.username,
        password = req.body.password;

    User.findOne({ username: username }, function(err, user) {
        if (err) {
            res.send(500, {
                success: false,
                message: 'An error occured'
            });
        } else {
            if (!user) {
                res.send(401, {
                    success: false,
                    message: 'Invalid username/password'
                });
            } else {
                if (!user.validPassword(password)) {
                    res.send(401, {
                        success: false,
                        message: 'Invalid username/password'
                    });
                } else {
                    user.currentToken = user.generateToken(user._id);
                    //user.currentToken = req.sessionID;
                    user.save();

                    console.log('sessionID: ' + req.sessionID);
                    //req.session.token = user.currentToken;

                    res.send({
                        success: true,
                        token: user.currentToken,
                        ownId: user._id
                    });
                }
            }
        }
    });
};

function validTokenProvided(req, res, callback) {
    // Check POST, GET, and headers for supplied token.
    var userToken = req.body.token || req.param('token') || req.headers.token;

    console.log('validate: ' + userToken);


    User.findOne({ _id: req.body.post.author }, function (err, user) {
        console.log('1.');
        if (err){
            console.log('error: ' + err);
            res.send(403, { error: 'Komischer Error. Invalid token. You provided: ' + userToken });
        } else if (user == null) {
            console.log('2.');
            res.send(403, { error: 'Invalid token. You provided: ' + userToken });
        } else {
            console.log('3.');
            if (user.currentToken == userToken) {
                console.log('4a.');
                callback();
            } else {
                console.log('4b.');
                res.send(403, { error: 'Invalid token. You provided: ' + userToken });
            }
        }
        console.log('5.');

    });
}
