App = Ember.Application.create();

App.Store = DS.Store.extend({
    revision: 11
});
App.ApplicationAdapter = DS.RESTAdapter.extend({
    host: 'http://myraspberry.no-ip.org:8080'
});

DS.RESTAdapter.reopen({
	headers: {
		"token": localStorage.getItem('token')
	}
});

App.PostSerializer = DS.RESTSerializer.extend({
    primaryKey: '_id'
});

App.CommentSerializer = DS.RESTSerializer.extend({
    primaryKey: '_id'
});


App.Router.map(function () {
    this.resource('posts');
    this.resource('post', { path: '/posts/:post_id'});
	this.resource('postNew');
    this.resource('login');
    this.resource('registration');
});
App.Post = DS.Model.extend({
    content: DS.attr('string'),
    title: DS.attr('string'),
    upVotes: DS.attr('number'),
    downVotes: DS.attr('number'),
    time: DS.attr('date'),
    author: DS.attr(),
    comments: DS.hasMany('Comment', {async:true})
	//__v: DS.attr('number')
});

App.Comment = DS.Model.extend({
    content: DS.attr('string'),
    title: DS.attr('string'),
    upVotes: DS.attr('number'),
    downVotes: DS.attr('number'),
    time: DS.attr('date'),
    author: DS.attr(),
    post: DS.belongsTo('Post')
});


App.PostsRoute = Ember.Route.extend({
    setupController: function (controller, model) {
        controller.set('model', model);
    },
    model: function (params) {
        return this.store.find('post');
    },
    actions: {

          newPost: function () {
           this.transitionToRoute('postNew');
        }
    }
});

App.PostRoute = Ember.Route.extend({
    model: function (params) {
        console.log(params.post_id);
        return this.store.find('post', params.post_id);
    }
});

App.PostsNewRoute=Ember.Route.extend({
  model: function(){
    return App.Post.createRecord();
  }
});

App.ApplicationController = Ember.Controller.extend({
    isLoggedIn: localStorage.getItem('isLoggedIn'),
    myid: '',
    mytoken: ''
});

App.PostController = Ember.ObjectController.extend({
    isEditing: false,
	newComment: false,

    actions: {
        edit: function () {
            this.set('isEditing', true);
        },
        doneEditing: function () {
            this.set('isEditing', false);
        },
		makeAComment: function(){
			this.set('newComment', true);
		},
		addComment: function(post){		
			console.log(id);
			var self = this,
				comment = this.store.createRecord('comment', {
					
                    content: this.get('contentComment'),
                    title: this.get('titleComment'),
                    upVotes: 0,
                    downVotes: 0,
                    time: new Date(),
                    author: localStorage.getItem('ownId')					
                })
            comment.save().then(function() {
            post.addObject(comment);
            // You may or may not need to save your post, too. In my case my backend handles 
            // the inverses of relationships (if existing), so there's no need. We still need 
            // to do this for Ember, though

        });   		
		}
    }
});

App.PostNewController = Ember.ObjectController.extend({
    needs: ['application'],
    isLoggedIn: Ember.computed.alias('controllers.application.isLoggedIn'),
    content: '',
    title: '',
    author: '',
    actions: {
        saveIt: function () {

            var self = this,
                post = this.store.createRecord('post', {
                    content: this.get('content'),
                    title: this.get('title'),
                    upVotes: 0,
                    downVotes: 0,
                    time: new Date(),
                    author: localStorage.getItem('ownId')
                })

            post.save().then(function (post) {
                console.log(post.get('id'));
                var temp = '/posts/' + post.get('id');
                self.transitionToRoute(temp);
            });
        }
    }
});

App.PostsController = Ember.ArrayController.extend({
    setupController: function(controller, model) {
        controller.set('content', model.get('posts'));
    },
    actions: {
        upvote: function (post) {
            console.log('upvote' + controller.getProperties('upVotes'));
        },
        downvote: function () {
            console.log('downvote');
        }
    }
});



App.LoginRoute = Ember.Route.extend({
    setupController: function(controller, context) {
        controller.reset();
    }
});

App.RegistrationRoute = Ember.Route.extend({
    setupController: function(controller, context) {
        controller.reset();
    }
});

App.LoginController = Ember.Controller.extend({
    needs: ['application'],
    isLoggedIn: Ember.computed.alias('controllers.application.isLoggedIn'),

    loginFailed: false,
    errorMessage: '',

    reset: function() {
        this.setProperties({
            username: "",
            password: "",
            errorMessage: ""
        });
    },
    ownId: localStorage.ownId,
    token: localStorage.token,

    tokenChanged: function() {
        localStorage.setItem('ownId', this.get('ownId'));
		localStorage.setItem('token', this.get('token'));
        localStorage.setItem('isLoggedIn', this.get('isLoggedIn'))

    }.observes('token'),

    actions: {
        login: function () {
            this.setProperties({
                loginFailed: false
            });

            var self = this,
                data = this.getProperties('username', 'password');

            this.set('errorMessage', null);

            $.post("http://myraspberry.no-ip.org:8080/login", data)
                .then(function (response) {

                    self.set('errorMessage', response.message);

                    if (response.success) {

                        console.log('success!');
                        self.set('ownId', response.ownId);
						self.set('token', response.token);
                        self.set('isLoggedIn', true);
						DS.RESTAdapter.reopen({
							headers: {
								"token": localStorage.getItem('token')
							}
						});
                    }
                });
        },
        logout: function() {
            isLoggedIn: false;
            token: null;
            ownId: null;
            localStorage.removeItem('ownId');
            localStorage.removeItem('token');
            localStorage.removeItem('isLoggedIn');
            location.reload();
        },
        checkToken: function() {
            console.log('Check token: ' + localStorage.getItem('token'));
        }

    }
});

App.RegistrationController = Ember.Controller.extend({
    errorMessage: '',

    reset: function() {
        this.setProperties({
            email: "",
            username: "",
            password: "",
            errorMessage: ""
        });
    },

    actions: {
        register: function () {
            this.setProperties({

            });

            var self = this,
                data = this.getProperties('username', 'password', 'email');

            this.set('errorMessage', null);

            $.post("http://myraspberry.no-ip.org:8080/users", data)
                .then(function (response) {

                    self.set('errorMessage', response.message);

                    if (response.success) {

                        console.log('success!');
                        self.transitionToRoute('login');
                    }

                });
        }
    }
});

Ember.Handlebars.helper('format-date', function (date) {
    return moment(date).fromNow();
});

var showdown = new Showdown.converter();

Ember.Handlebars.helper('format-markdown', function (input) {
    //return new Handlebars.SafeString(showdown.makeHtml(input));
    return input;
});

// DateFormat: var d = new Date(2011, 01, 07); yyyy, mm-1, dd --> To work in Safari